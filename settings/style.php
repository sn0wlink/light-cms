/*

	CSS STYLESHEET
===================================


	Sn0wlink IT
	-------------------------------
	Author:David Collins-Cubitt
	Designed By : David Collins-Cubitt
	Copyright Sn0wlink IT 2016
	http://www.sn0wlink.com/
	
===================================
	
*/

<?php	include ('config.php');?>

.background {
	z-index:-1;
	top:0;
	left:0;
	width: 100%;
	position:fixed;
}

h1 {
	color: <?php echo $webcolour1; ?>;
	
}

h2 {
	color: <?php echo $webcolour1; ?>;
	
}

h3 {
	color: <?php echo $webcolour1; ?>;
	
}

.logo {
	text-align:center;
	font-size: 42pt;
	font-weight: bold;
	padding: 60px;
	color:#ffffff;
}

.menu {
	text-align: right;
	background-color: <?php echo $webcolour2; ?>;
	padding:10px;
}

.menu a {
	color: #ffffff;
	font: arial;
	padding: 5px;
	font-size: 11pt;
	font-weight: bold;
	text-decoration: none;
}

a {
	color: blue;
	text-decoration: none;
	font-weight:bold;
}

body {
	font-family:arial;
	background-color:#000000;
}

.container {
	width:<?php echo $pagewidth; ?>;
	margin: auto;
	background-color: #ffffff;
	border-style: solid;
	border-width: 0.7px;
	border-color:<?php echo $webcolour1; ?>;
	opacity:20%;
}

.content {
	color:<?php echo $textcolour; ?>;
	font-size:11pt;
	padding: 20px;
	margin-left:10px;
}


.footer {
	padding:10px;
	color: #ffffff;
	background-color: <?php echo $webcolour2; ?>;
	font-size: 9pt;
	text-align: center;
}

