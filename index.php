<html>

<head>

	<link rel="stylesheet" type="text/css" href="settings/style.php">

	<?php
	// Configuration file
	include ('settings/config.php');
	?>

</head>

<title> 
	<?php  
	print $sitename;
	?> 
</title>

<img class='background' src='settings/images/background.jpg' />

	<div class='logo'><?php echo $sitename; ?></div>

<body>
	
<div class='container'>
	


	<div class='menu'>
	<?php include ('settings/menu.php') ?>
	</div>

	<div class='content'>
	<h3>Welcome to <?php echo $sitename; ?></h3>
	<p></p>
	<?php include ('content/home.txt'); ?>
	</div>

	<div class='footer'>
	<?php include ('settings/footer.php') ?>
	</div>
</div>

</body>

</html>
